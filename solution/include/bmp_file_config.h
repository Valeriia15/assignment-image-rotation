#pragma once
#include <stdint.h>
#include <stdlib.h>

#pragma pack(push, 1)
struct BmpHeader {
        uint16_t bfType;
        uint32_t bfileSize;
        uint32_t bfReserved;
        uint32_t bOffBits;
        uint32_t biSize;
        uint32_t biWidth;
        uint32_t biHeight;
        uint16_t biPlanes;
        uint16_t biBitCount;
        uint32_t biCompression;
        uint32_t biSizeImage;
        uint32_t biXPelsPerMeter;
        uint32_t biYPelsPerMeter;
        uint32_t biClrUsed;
        uint32_t biClrImportant;
};
#pragma pack(pop)

struct Pixel { uint8_t b, g, r; };

struct Image {
	uint64_t width, height;

	struct Pixel* data;
};

static const uint16_t BMP_FILE_TYPE = 19778;

uint32_t getPadding(const uint32_t w, const uint32_t h);
