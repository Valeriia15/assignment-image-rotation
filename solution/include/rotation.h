#include "bmp_file_config.h"

struct Image getNewImage(struct Image const source);

void rotate(struct Image *newImage, struct Image const source);

