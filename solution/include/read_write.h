#include "bmp_file_config.h"
#include <stdio.h>

enum ReadStatus  {
	READ_OK = 0,
	READ_INVALID_SIGNATURE,
	READ_INVALID_BITS,
	READ_INVALID_HEADER,
	READ_INVALID_INPUT_PARAMS,
	READ_CALLOC_ERROR
 };


enum  WriteStatus  {
	WRITE_OK = 0,
	WRITE_ERROR
};

enum ReadStatus FromBmp(FILE *in, struct Image* img, struct BmpHeader *header);

enum WriteStatus ToBmp(FILE *out, struct Image const* img, struct BmpHeader *header);

enum ReadStatus ReadBmp (const char *inFileName, struct Image* img, struct BmpHeader *header);

enum WriteStatus WriteBmp(const char *outFileName, struct Image* img, struct BmpHeader *header);

struct BmpHeader getBmpHeader(struct Image *image, struct BmpHeader  oldHeader);
