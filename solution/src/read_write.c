#include "../include/read_write.h"

enum ReadStatus FromBmp(FILE *in, struct Image* img, struct BmpHeader *header){
	if ( in    == NULL) return READ_INVALID_INPUT_PARAMS;
	if ( img   == NULL) return READ_INVALID_INPUT_PARAMS;
	if (header == NULL) return READ_INVALID_INPUT_PARAMS;
	
	fread(header, sizeof(struct BmpHeader), 1, in);
	if (header == NULL) return READ_INVALID_HEADER;

	img->width  = header->biWidth;
	img->height = header->biHeight;

	uint64_t bytesPerLine = ((img->width * header->biBitCount + 31) >> 5) << 2;

	uint64_t imageSize = bytesPerLine * img->height;

	img -> data = (struct Pixel *) malloc(imageSize);
	if (img -> data == NULL) return READ_CALLOC_ERROR;

	fread(img->data, sizeof(char), imageSize, in);
	if (header == NULL) return READ_INVALID_BITS;

	return READ_OK;
}

enum WriteStatus ToBmp(FILE* out, struct Image const* img, struct BmpHeader *header) {
	if (out == NULL) return WRITE_ERROR;
	if (img == NULL) return WRITE_ERROR;

	if (fwrite(header, sizeof(struct BmpHeader), 1, out) != 1) {
		fprintf(stderr, "fwrite error\n");
		return WRITE_ERROR;
	}

	if (fwrite(img->data, sizeof(char), header->biSizeImage, out) != header->biSizeImage) {
		fprintf(stderr, "fwrite error\n");
		return WRITE_ERROR;
	}

	return WRITE_OK;
}

enum ReadStatus ReadBmp(const char* inFileName, struct Image* img, struct BmpHeader* header) {
	if (inFileName == NULL) return READ_INVALID_INPUT_PARAMS;
	if (img        == NULL) return READ_INVALID_INPUT_PARAMS;
	if (header     == NULL) return READ_INVALID_INPUT_PARAMS;

	FILE *inFile = fopen(inFileName, "rb");
	if (inFile == NULL) return READ_INVALID_INPUT_PARAMS;

	enum ReadStatus status = FromBmp(inFile, img, header);
	fclose(inFile);

	return status;
}

struct BmpHeader getBmpHeader(struct Image *image, struct BmpHeader oldHeader) {
	struct BmpHeader newHeader= {0};

	newHeader.bfType      = (uint16_t) BMP_FILE_TYPE;
	newHeader.bfileSize   = (uint32_t) getPadding(image->width, image->height) + sizeof(struct BmpHeader);
	newHeader.bOffBits    = (uint32_t) sizeof(struct BmpHeader);
	newHeader.biWidth     = (uint32_t) image -> width;
	newHeader.biHeight    = (uint32_t) image -> height;
	newHeader.biSizeImage = (uint32_t) getPadding(image->width, image->height);
	newHeader.bfReserved  = 0;

	newHeader.biXPelsPerMeter = oldHeader.biYPelsPerMeter;
	newHeader.biYPelsPerMeter = oldHeader.biXPelsPerMeter;
	newHeader.biSize 		  = oldHeader.biSize;
	newHeader.biCompression   = oldHeader.biCompression;
	newHeader.biClrImportant  = oldHeader.biClrImportant;
	newHeader.biClrUsed 	  = oldHeader.biClrUsed;
	newHeader.biPlanes		  = oldHeader.biPlanes;
	newHeader.biBitCount	  = oldHeader.biBitCount;

	return newHeader;
}

enum WriteStatus WriteBmp(const char* outFileName, struct Image* img, struct BmpHeader* header) {
	if (outFileName == NULL) return WRITE_ERROR;
	if (img         == NULL) return WRITE_ERROR;
	if (header      == NULL) return WRITE_ERROR;

	FILE *outFile = fopen(outFileName, "wb");
	if (outFile == NULL) return WRITE_ERROR;

	enum WriteStatus status = ToBmp(outFile, img, header);
	fclose(outFile);

	return status;
}
