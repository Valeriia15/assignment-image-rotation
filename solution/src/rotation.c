#include "../include/rotation.h"
#include <stdio.h>

uint32_t getCurString(const uint32_t width, const uint32_t y);

struct Image getNewImage(struct Image const source) {
	struct Image newImage;
	
	newImage.height = source.width;
	newImage.width  = source.height;

	size_t newImageDataSize = (newImage.width * 3 + getPadding(newImage.width, newImage.height)) * newImage.height;
	newImage.data = (struct Pixel *) calloc(newImageDataSize / 4, sizeof(struct Pixel));

	if (newImage.data == NULL) {
		fprintf(stderr, "Nullptr in newImage.data\n");
		return newImage;
	}

	rotate(&newImage, source);

	return newImage;
}

void rotate(struct Image *newImage, struct Image const source) {
	if (newImage == NULL) return;
	
	for (uint32_t y = 0; y < source.height; y++)
		for (uint32_t x = 0; x < source.width; x++) {
			uint32_t curX = newImage->width - y - 1;
		
			struct Pixel *sourcePixel_ptr   = (struct Pixel *)((char *)    source.data + getCurString(   source.width,    y) +    x * 3);
			struct Pixel *newImagePixel_ptr = (struct Pixel *)((char *) newImage->data + getCurString(newImage->width,    x) + curX * 3);

			// fprintf(stderr, "%u -> %u\n", getCurString(  source.width,    y) + x * 3, getCurString(newImage.width, curX) + y * 3);
			// fprintf(stderr, "%u - %u\n\n", x, y);0
			*newImagePixel_ptr = *sourcePixel_ptr;
		}
	// fprintf(stderr, "Here\n");
}

uint32_t getCurString(const uint32_t width, const uint32_t y) {
	uint32_t string_len = (width * 3 + 3) / 4 * 4;
	
	return y * string_len;
}

uint32_t getPadding(const uint32_t w, const uint32_t h) {
	return ((w * 3 + 3) / 4 * 4) * h;
}
