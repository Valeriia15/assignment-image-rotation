#include "../include/read_write.h"
#include "../include/rotation.h"
#include <stdio.h>

int main( int argc, char** argv ) {
	if (argc != 3) {
		fprintf(stderr, "args are invalid\n");
		return 0;
	}

	struct Image image;
	struct BmpHeader header = {0};

	enum ReadStatus readStatus = ReadBmp(argv[1], &image, &header);
	if (readStatus) {
		fprintf(stderr, "%d error occured\n", readStatus);
		return 0;
	}

	struct Image newImage = getNewImage(image);

	struct BmpHeader newHeader = getBmpHeader(&newImage, header);

	enum WriteStatus writeStatus = WriteBmp(argv[2], &newImage, &newHeader);
	if (writeStatus) {
		fprintf(stderr, "%d error occured\n", writeStatus);
		return 0;
	}


	free(   image.data);
	free(newImage.data);

    return 0;
}
